       IDENTIFICATION DIVISION. 
       PROGRAM-ID. TRADER.
       AUTHOR. WATCHARAPHON.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT TRADER-FILE ASSIGN TO "trader9.dat"
              ORGANIZATION IS LINE SEQUENTIAL.
           
           SELECT TRADER-RPT-FILE ASSIGN TO "trader1.rpt"
              ORGANIZATION IS SEQUENTIAL.
           
       DATA DIVISION. 
       FILE SECTION. 
       FD  TRADER-FILE.
       01  TRANDER-DETAIL.
           88 END-OF-FILE VALUE HIGH-VALUE .
           05 TRADER-PROVINCE         PIC 9(2).
           05 TRADER-ID               PIC 9(4).
           05 TRADER-INCOME           PIC 9(6).

       FD  TRADER-RPT-FILE.
       01  PRN-LINE             PIC x(44).

       WORKING-STORAGE SECTION. 
       01  COL-HEADING                PIC X(50)  
           VALUE "PROVINCE    P INCOME    MEMBER  MEMBER INCOME".  

       01  MAX-TOTAL-DETIL.
           05 MAX-PROVINCE            PIC 9(2).
           05 MAX-SUM-INCOME          PIC 9(9)V9(3).  

       01  COL-P-PROCESSING           PIC 9(2).
       01  COL-P-INCOME               PIC 9(9).  
       01  COL-P-MEMBER               PIC 9(4).
       01  COL-P-MEMBER-INCOME        PIC 9(6) VALUE ZERO .
       
       01  RPT-DETAIL.
           05  RPT-PROCESSING             PIC 99.
           05  RPT-INCOME                 PIC $$$$,$$$,$$9.
           05  RPT-MEMBER                 PIC 9(4).
           05  RPT-MENBER-INCOME          PIC $$$$,$$$,$$9.
       
       PROCEDURE DIVISION .
       BEGIN.
           OPEN INPUT TRADER-FILE
           OPEN OUTPUT TRADER-RPT-FILE

           PERFORM READ-FILE 
           PERFORM PROCESS-COL UNTIL END-OF-FILE 
           
           DISPLAY RPT-DETAIL
      *    PERFORM WRITE-FILE 
           CLOSE TRADER-FILE 
           CLOSE TRADER-RPT-FILE
           GOBACK
           .
       PROCESS-COL.
           MOVE TRADER-ID TO COL-P-PROCESSING
           MOVE ZERO TO COL-P-INCOME
           MOVE ZERO TO COL-P-MEMBER-INCOME

           PERFORM PROCESS-LINE 
              UNTIL TRADER-PROVINCE NOT = COL-P-PROCESSING

           MOVE COL-P-PROCESSING TO RPT-PROCESSING
           MOVE COL-P-INCOME TO RPT-INCOME
           MOVE COL-P-MEMBER TO RPT-MEMBER
           MOVE COL-P-MEMBER-INCOME TO RPT-MENBER-INCOME 
           
       .
       
       PROCESS-LINE.
           ADD TRADER-INCOME TO COL-P-INCOME

           PERFORM READ-FILE 
           .

       READ-FILE.
           READ TRADER-FILE
              AT END SET END-OF-FILE TO TRUE
           END-READ
       .
      *WRITE-FILE.
      *    WRITE PRN-LINE FROM RPT-DETAIL.